#----------------------------------
# PURPOSE
#---------------------------------- 
# To install a pkcs#12 and CA certificate into the local machine store.
# These are retrieved from the nomad endpoint
#----------------------------------
# FUNCTIONS
#---------------------------------- 

    function IsAdmin
    {
        $currentPrincipal = New-Object Security.Principal.WindowsPrincipal( [Security.Principal.WindowsIdentity]::GetCurrent() )
        & {
            if (!$currentPrincipal.IsInRole( [Security.Principal.WindowsBuiltInRole]::Administrator ))
            {        
                write "Warning: PowerShell is not running as an Administrator.`n"
                exit
            }
                    
            write "Running as an Administrator.`n"            
        }
    }

    function DisplaySecurityWarning
    { 
        cls
        write "
        `n                   ***** This service is for authorised clients only *****

         ****************************************************************************
         * WARNING:    It is a criminal offence to:                                 *
         *        i. Obtain access to data without authority                        *
         *            (Penalty 2 years imprisonment)                                *
         *        ii Damage, delete, alter or insert data without authority         *
         *            (Penalty 10 years imprisonment)                               *
         ****************************************************************************"
    }

    function IgnoreSLL
    {
	    $Provider = New-Object Microsoft.CSharp.CSharpCodeProvider
	    $Compiler= $Provider.CreateCompiler()
	    $Params = New-Object System.CodeDom.Compiler.CompilerParameters
	    $Params.GenerateExecutable = $False
	    $Params.GenerateInMemory = $True
	    $Params.IncludeDebugInformation = $False
	    $Params.ReferencedAssemblies.Add("System.DLL") > $null	
        $TASource="
		namespace Local.ToolkitExtensions.Net.CertificatePolicy
		{
			public class TrustAll : System.Net.ICertificatePolicy
			{
				public TrustAll() {}
				public bool CheckValidationResult(System.Net.ServicePoint sp,System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Net.WebRequest req, int problem)
				{
					return true;
				}
			}
		}"

	    $TAResults=$Provider.CompileAssemblyFromSource($Params,$TASource)
	    $TAAssembly=$TAResults.CompiledAssembly
        
        ## We create an instance of TrustAll and attach it to the ServicePointManager
	    $TrustAll = $TAAssembly.CreateInstance("Local.ToolkitExtensions.Net.CertificatePolicy.TrustAll")
            [System.Net.ServicePointManager]::CertificatePolicy = $TrustAll
    }

    function VerifyHost
    {
        $hostnamePattern = '^[a-z]{2,3}[0-9]{4,8}$'
        if ($env:COMPUTERNAME -match $hostnamePattern)
        {
            write "Valid Host: " + $env:COMPUTERNAME + "`n"
            return
        }       

        write "Invalid Asset`n"
        exit 
    }
    
    function ShowInstalledCerts ($store, $pattern)
    {
        $currentStore = new-object System.Security.Cryptography.X509Certificates.X509Store $store, 'LocalMachine'
        $currentStore.Open("MaxAllowed")
                
        foreach ($cert in $currentStore.Certificates)
        {
           if ($cert.Subject -like $pattern)
           {
                write 'Installed: (' + $store + ') '  + $cert.Subject
           }
        }

        $currentStore.Close()        
    }

    function PurgeCerts ($store, $pattern)
    {        
        $currentStore = new-object System.Security.Cryptography.X509Certificates.X509Store $store, 'LocalMachine'
        $currentStore.Open("MaxAllowed")
                
        foreach ($cert in $currentStore.Certificates)
        {
           if ($cert.Subject -like $pattern)
           {
                write 'Removing: (' + $store + ') '  + $cert.Subject
                $currentStore.Remove($cert)
           }
        }

        $currentStore.Close()        
    }

    Function AddCert ($cert, $store)
    {    
        $store = new-object System.Security.Cryptography.X509Certificates.X509Store $store, 'LocalMachine'
        $store.open('MaxAllowed')    
        $store.add($cert)    
        $store.close()                    
    }
      
    function Decode ($secureString)
    {
        return [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureString))
    }
     
#---------------------------------
# VARIABLES
#---------------------------------

    # Nomad URL
    $nomadURL = 'https://10.**.**.**/servlets/Enrollment?action='
    
    # Old cert patterns
    $authorityMatchPattern = '*cn=nomad.*'
    $deviceMatchPattern = '*O=Health, OU=Nomad, OU=Device, CN=' + $env:COMPUTERNAME + '*'

    # Cert url actions
    $authorityCert = $nomadURL + 'ca'
    $createDeviceCert = $nomadURL + 'create&dn=' + $env:COMPUTERNAME + '&entityType=DEVICE'
    $deviceCert = $nomadURL + 'p12&dn=cn=' + $env:COMPUTERNAME  + ',ou=Device,ou=Nomad,o=Health'

    # Temp cert locations
    $authorityCertFile = 'C:\authorityCert.cer'
    $deviceCertFile = [String]::Format('C:\DeviceCert-{0}.p12', $env:COMPUTERNAME)
    
    # Set to true so ssl does not cause fault
    # Replaced with ignore_SLL as the line cannot be set from a soe
    # [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}    
    IgnoreSLL

    # Build web client object to interogate server
    $wc = New-Object Net.Webclient  
    $wc.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)")    
    
#---------------------------------
# MAIN
#---------------------------------

function Main 
{      
    DisplaySecurityWarning

    IsAdmin    

    VerifyHost

    $username = Read-Host 'Username'
    $password = Decode(Read-Host -assecurestring 'Password')  

    # Configure webclient with creds    
    $wc.Credentials = new-object System.Net.NetworkCredential($username, $password)
    
    PurgeCerts 'MY' $deviceMatchPattern
    PurgeCerts 'ROOT' $authorityMatchPattern
                        
    $wc.DownloadFile($authorityCert, $authorityCertFile)                                        
    $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2 $authorityCertFile
    write 'Step 1 out of 5: CA certificate retrieved'                  
        
    AddCert $cert 'Root' 
    write 'Step 2 out of 5: CA certificate added'
   
    $networkKey = $wc.DownloadString($createDeviceCert).Split('	')[1]    
    write 'Step 3 out of 5: Device certificate created'    
                              
    $wc.DownloadFile($deviceCert, $deviceCertFile)  
    write 'Step 4 out of 5: Retrieving the device certificate'         
    
    $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2      
    $cert.import($deviceCertFile, $networkKey, 'Exportable,PersistKeySet')                            
    AddCert $cert 'My'                 
    write  "Step 5 out of 5: Device certificate retrieved and added`n"
   
    rm $deviceCertFile
    rm $authorityCertFile
    
    ShowInstalledCerts 'MY' $deviceMatchPattern
    ShowInstalledCerts 'ROOT' $authorityMatchPattern

    # Handle Errors Correctly (would use try catch but this does 
    # not execute on XP machines)
    trap [Exception] 
    {    
        $errorMsg = $_.Exception.Message
        if ($errorMsg -like '*(401)*')
        {
            write "The remote server returned an error: (401) Unauthorized."
        }    
        elseif ($errorMsg -like '*The system cannot find the file specified.*')
        {
            write "(Failid to build cert) The system cannot find the file specified."
        }
        else
        {               
            $debug = $_ |select -expandproperty invocationinfo 
            write `n----------------------------------------------------------
            write "Application has terminated!"      
            write ----------------------------------------------------------`n                           
            write ("Type : " + $_.Exception.GetType().FullName)                                 
            write $debug | findstr 'PositionMessage'
            write $debug | findstr /B 'Line' 
        }           
        exit                                                   
    }
}

Main